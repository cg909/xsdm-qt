xSDM-qt
=======

A GUI for [v3l0c1r4pt0r's xSDM][xSDM].

It automates the downloading, key fetching and unpacking process of SDX files.

SDX is used by kivuto's SDM (Secure Download Manager) – a program needed to
download from sites like MSDNAA/Dreamspark.

Prerequisites
-------------

You need the following packages:

* python3 (>= 3.5)
* python3-pyqt4
* pyqt4-dev-tools / PyQt4-devel
* [xSDM][]

Installation
------------

Run `make` in the project root directory to generate all needed files.

Then use `make install` to install.

The following environment variables are used:

+ **PREFIX**: installation prefix (defaults to /usr/local)
+ **PYUIC4**: path to pyuic4 (if not found automatically)

[xSDM]: https://github.com/v3l0c1r4pt0r/xSDM