#!/usr/bin/make -f
# (c) 2016 Christoph Grenz <christophg@grenz-bonn.de>
# License: GNU GPLv3 or later
# This file is part of xsdm-qt

PYUIC4 ?= pyuic4
PREFIX ?= /usr/local

all: check xsdm-qt.py

check:
	@if [ "${PYUIC4}" = "pyuic4" ] && ! which "pyuic4" >/dev/null; then \
		echo "ERROR: Please install 'pyuic4' (should be in packet pyqt4-dev-tools or PyQt4-devel)"; exec false; \
	fi
	@if ! which "python3" >/dev/null; then \
		echo "WARNING: Please install 'python3' (at least version 3.5)"; \
	fi
	@if ! which "python3" >/dev/null || ! python3 -c 'import typing' 2>/dev/null; then \
		echo "WARNING: Please upgrade to a newer Python version or install the 'typing' module"; \
	fi

xsdm-qt.py: mainwindow.py Makefile

%.py: %.ui
	${PYUIC4} "$<" > "$@"

run: xsdm-qt.py
	"./$<"

clean:
	rm -f mainwindow.py

install: xsdm-qt.py
	install -d "${PREFIX}/lib/xsdm-qt"
	install -m 755 -t "${PREFIX}/lib/xsdm-qt" "xsdm-qt.py"
	install -m 644 -t "${PREFIX}/lib/xsdm-qt" "mainwindow.py"
	ln -s '../lib/xsdm-qt/xsdm-qt.py' "${PREFIX}/bin/xsdm-qt"
	install -m 755 "xsdm-qt.png" "${PREFIX}/share/pixmaps/"
	ln -s '../pixmaps/xsdm-qt.png' "${PREFIX}/share/icons/xsdm-qt.png"
	xdg-desktop-menu install --mode system --novendor xsdm-qt.desktop
	xdg-mime install --mode system --novendor ./mime-info.xml
	

uninstall:
	xdg-mime uninstall --mode system --novendor ./mime-info.xml || true
	xdg-desktop-menu uninstall  --mode system --novendor xsdm-qt.desktop
	rm "${PREFIX}/share/pixmaps/xsdm-qt.png" || true
	rm "${PREFIX}/share/icons/xsdm-qt.png" || true
	rm "${PREFIX}/lib/xsdm-qt/xsdm-qt.py" "${PREFIX}/lib/xsdm-qt/mainwindow.py"
	rmdir "${PREFIX}/lib/xsdm-qt" || true
	rm "${PREFIX}/bin/xsdm-qt"

.PHONY: all check run clean install
