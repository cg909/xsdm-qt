#!/usr/bin/python3
# (c) 2016 Christoph Grenz <christophg@grenz-bonn.de>
# License: GNU GPLv3 or later
# This file is part of xsdm-qt

import os, io, time, re
from PyQt4 import QtGui, Qt
from urllib.parse import urlparse
from base64 import b64encode, b64decode
from typing import *
from typing.io import *
import xml.etree.ElementTree as ET
import urllib.parse
import mainwindow
import logging
import argparse
import subprocess
import functools
import json
import xattr
import webbrowser
from collections import deque
import ctypes
import ctypes.util

c_off_t = ctypes.c_int64

loadergif = b'''R0lGODlhIAAgAPMAAP///wAAAMbGxoSEhLa2tpqamjY2NlZWVtjY2OTk5Ly8vB4eHgQEBAAAAAAA
AAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJ
CgAAACwAAAAAIAAgAAAE5xDISWlhperN52JLhSSdRgwVo1ICQZRUsiwHpTJT4iowNS8vyW2icCF6
k8HMMBkCEDskxTBDAZwuAkkqIfxIQyhBQBFvAQSDITM5VDW6XNE4KagNh6Bgwe60smQUB3d4Rz1Z
BApnFASDd0hihh12BkE9kjAJVlycXIg7CQIFA6SlnJ87paqbSKiKoqusnbMdmDC2tXQlkUhziYty
WTxIfy6BE8WJt5YJvpJivxNaGmLHT0VnOgSYf0dZXS7APdpB309RnHOG5gDqXGLDaC457D1zZ/V/
nmOM82XiHRLYKhKP1oZmADdEAAAh+QQJCgAAACwAAAAAIAAgAAAE6hDISWlZpOrNp1lGNRSdRpDU
olIGw5RUYhhHukqFu8DsrEyqnWThGvAmhVlteBvojpTDDBUEIFwMFBRAmBkSgOrBFZogCASwBDEY
/CZSg7GSE0gSCjQBMVG023xWBhklAnoEdhQEfyNqMIcKjhRsjEdnezB+A4k8gTwJhFuiW4dokXil
oUepBAp5qaKpp6+Ho7aWW54wl7obvEe0kRuoplCGepwSx2jJvqHEmGt6whJpGpfJCHmOoNHKaHx6
1WiSR92E4lbFoq+B6QDtuetcaBPnW6+O7wDHpIiK9SaVK5GgV543tzjgGcghAgAh+QQJCgAAACwA
AAAAIAAgAAAE7hDISSkxpOrN5zFHNWRdhSiVoVLHspRUMoyUakyEe8PTPCATW9A14E0UvuAKMNAZ
KYUZCiBMuBakSQKG8G2FzUWox2AUtAQFcBKlVQoLgQReZhQlCIJesQXI5B0CBnUMOxMCenoCfTCE
WBsJColTMANldx15BGs8B5wlCZ9Po6OJkwmRpnqkqnuSrayqfKmqpLajoiW5HJq7FL1Gr2mMMcKU
MIiJgIemy7xZtJsTmsM4xHiKv5KMCXqfyUCJEonXPN2rAOIAmsfB3uPoAK++G+w48edZPK+M6hLJ
pQg484enXIdQFSS1u6UhksENEQAAIfkECQoAAAAsAAAAACAAIAAABOcQyEmpGKLqzWcZRVUQnZYg
1aBSh2GUVEIQ2aQOE+G+cD4ntpWkZQj1JIiZIogDFFyHI0UxQwFugMSOFIPJftfVAEoZLBbcLEFh
lQiqGp1Vd140AUklUN3eCA51C1EWMzMCezCBBmkxVIVHBWd3HHl9JQOIJSdSnJ0TDKChCwUJjoWM
PaGqDKannasMo6WnM562R5YluZRwur0wpgqZE7NKUm+FNRPIhjBJxKZteWuIBMN4zRMIVIhffcgo
jwCF117i4nlLnY5ztRLsnOk+aV+oJY7V7m76PdkS4trKcdg0Zc0tTcKkRAAAIfkECQoAAAAsAAAA
ACAAIAAABO4QyEkpKqjqzScpRaVkXZWQEximw1BSCUEIlDohrft6cpKCk5xid5MNJTaAIkekKGQk
WyKHkvhKsR7ARmitkAYDYRIbUQRQjWBwJRzChi9CRlBcY1UN4g0/VNB0AlcvcAYHRyZPdEQFYV8c
cwR5HWxEJ02YmRMLnJ1xCYp0Y5idpQuhopmmC2KgojKasUQDk5BNAwwMOh2RtRq5uQuPZKGIJQIG
wAwGf6I0JXMpC8C7kXWDBINFMxS4DKMAWVWAGYsAdNqW5uaRxkSKJOZKaU3tPOBZ4DuK2LATgJhk
PJMgTwKCdFjyPHEnKxFCDhEAACH5BAkKAAAALAAAAAAgACAAAATzEMhJaVKp6s2nIkolIJ2WkBSh
pkVRWqqQrhLSEu9MZJKK9y1ZrqYK9WiClmvoUaF8gIQSNeF1Er4MNFn4SRSDARWroAIETg1iVwuH
jYB1kYc1mwruwXKC9gmsJXliGxc+XiUCby9ydh1sOSdMkpMTBpaXBzsfhoc5l58Gm5yToAaZhaOU
qjkDgCWNHAULCwOLaTmzswadEqggQwgHuQsHIoZCHQMMQgQGubVEcxOPFAcMDAYUA85eWARmfSRQ
CdcMe0zeP1AAygwLlJtPNAAL19DARdPzBOWSm1brJBi45soRAWQAAkrQIykShQ9wVhHCwCQCACH5
BAkKAAAALAAAAAAgACAAAATrEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA
7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyND
J0uIiRMDjI0Fd30/iI2UA5GSS5UDj2l6NoqgOgN4gksEBgYFf0FDqKgHnyZ9OX8HrgYHdHpcHQUL
XAS2qKpENRg7eAMLC7kTBaixUYFkKAzWAAnLC7FLVxLWDBLKCwaKTULgEwbLA4hJtOkSBNqITT3x
EgfLpBtzE/jiuL04RGEBgwWhShRgQExHBAAh+QQJCgAAACwAAAAAIAAgAAAE7xDISWlSqerNpyJK
hWRdlSAVoVLCWk6JKlAqAavhO9UkUHsqlE6CwO1cRdCQ8iEIfzFVTzLdRAmZX3I2SfZiCqGk5dTE
SJeaOAlClzsJsqwiJwiqnFrb2nS9kmIcgEsjQydLiIlHehhpejaIjzh9eomSjZR+ipslWIRLAgMD
OR2DOqKogTB9pCUJBagDBXR6XB0EBkIIsaRsGGMMAxoDBgYHTKJiUYEGDAzHC9EACcUGkIgFzgwZ
0QsSBcXHiQvOwgDdEwfFs0sDzt4S6BK4xYjkDOzn0unFeBzOBijIm1Dgmg5YFQwsCMjp1oJ8LyIA
ACH5BAkKAAAALAAAAAAgACAAAATwEMhJaVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqU
ToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyA
SyNDJ0uIiUd6GGl6NoiPOH16iZKNlH6KmyWFOggHhEEvAwwMA0N9GBsEC6amhnVcEwavDAazGwID
aH1ipaYLBUTCGgQDA8NdHz0FpqgTBwsLqAbWAAnIA4FWKdMLGdYGEgraigbT0OITBcg5QwPT4xLr
ROZL6AuQAPUS7bxLpoWidY0JtxLHKhwwMJBTHgPKdEQAACH5BAkKAAAALAAAAAAgACAAAATrEMhJ
aVKp6s2nIkqFZF2VIBWhUsJaTokqUCoBq+E71SRQeyqUToLA7VxF0JDyIQh/MVVPMt1ECZlfcjZJ
9mIKoaTl1MRIl5o4CUKXOwmyrCInCKqcWtvadL2SYhyASyNDJ0uIiUd6GAULDJCRiXo1CpGXDJOU
jY+Yip9DhToJA4RBLwMLCwVDfRgbBAaqqoZ1XBMHswsHtxtFaH1iqaoGNgAIxRpbFAgfPQSqpbgG
BqUD1wBXeCYp1AYZ19JJOYgH1KwA4UBvQwXUBxPqVD9L3sbp2BNk2xvvFPJd+MFCN6HAAIKgNggY
0KtEBAAh+QQJCgAAACwAAAAAIAAgAAAE6BDISWlSqerNpyJKhWRdlSAVoVLCWk6JKlAqAavhO9Uk
UHsqlE6CwO1cRdCQ8iEIfzFVTzLdRAmZX3I2SfYIDMaAFdTESJeaEDAIMxYFqrOUaNW4E4ObYcCX
aiBVEgULe0NJaxxtYksjh2NLkZISgDgJhHthkpU4mW6blRiYmZOlh4JWkDqILwUGBnE6TYEbCgev
r0N1gH4At7gHiRpFaLNrrq8HNgAJA70AWxQIH1+vsYMDAzZQPC9VCNkDWUhGkuE5PxJNwiUK4UfL
zOlD4WvzAHaoG9nxPi5d+jYUqfAhhykOFwJWiAAAIfkECQoAAAAsAAAAACAAIAAABPAQyElpUqnq
zaciSoVkXVUMFaFSwlpOCcMYlErAavhOMnNLNo8KsZsMZItJEIDIFSkLGQoQTNhIsFehRww2CQLK
F0tYGKYSg+ygsZIuNqJksKgbfgIGepNo2cIUB3V1B3IvNiBYNQaDSTtfhhx0CwVPI0UJe0+bm4g5
VgcGoqOcnjmjqDSdnhgEoamcsZuXO1aWQy8KAwOAuTYYGwi7w5h+Kr0SJ8MFihpNbx+4Erq7BYBu
zsdiH1jCAzoSfl0rVirNbRXlBBlLX+BP0XJLAPGzTkAuAOqb0WT5AH7OcdCm5B8TgRwSRKIHQtaL
Cwg1RAAAOwAAAAAAAAAAAA=='''

def make_fallocate():
	libc_name = ctypes.util.find_library('c')
	libc = ctypes.CDLL(libc_name)

	_fallocate = libc.fallocate
	_fallocate.restype = ctypes.c_int
	_fallocate.argtypes = [ctypes.c_int, ctypes.c_int, c_off_t, c_off_t]

	del libc
	del libc_name

	def fallocate(fd, mode, offset, len_):
		res = _fallocate(fd.fileno(), mode, offset, len_)
		if res != 0:
			raise IOError(res, 'fallocate')

	return fallocate

fallocate = make_fallocate()
del make_fallocate

FALLOC_FL_KEEP_SIZE = 0x01
FALLOC_FL_PUNCH_HOLE = 0x02

logger = logging.getLogger('qxsdm')

URL = NewType('URL', str)

@functools.lru_cache()
def getXDGDownloadDir() -> str:
	try:
		return subprocess.check_output(['xdg-user-dir', 'DOWNLOAD']).decode().rstrip('\r\n')
	except subprocess.CalledProcessError:
		return os.environ.get('HOME', '/')

def setDownloadDir(path: str) -> None:
	global _downloadDir
	_downloadDir = path

def getDownloadDir() -> str:
	global _downloadDir
	try:
		return _downloadDir
	except NameError:
		return getXDGDownloadDir()

class ErrorDialogHandler(logging.Handler):
	
	def __init__(self, parent: Qt.QObject=None, level:int=logging.ERROR) -> None:
		super().__init__(level)
		self.parent = parent
	
	def emit(self, record) -> None:
		QtGui.QMessageBox.critical(self.parent, None, self.format(record))
	
	def close(self) -> None:
		self.parent = None

class Downloader(Qt.QObject):
	
	def __init__(self, parent: 'SDMViewHandler', file: dict) -> None:
		super().__init__(parent)
		self.networkaccess = parent.networkaccess
		self.request = None
		self.response = None
		self.fh = None
		self.file = file
		self.queue = deque(file['fileParts'])
		self.proc = None
		self.tstamp = 0
	
	def start(self):
		self._recalcTotalLength()
		self.done_length = 0
		# Check if download already complete
		if os.path.exists(self.file['filePath']):
			self.queue.clear()
		# If the key is missing, add it to queue
		if not self.file['edv']:
			url = self.file['hfBaseUrl'] + '?' + urllib.parse.urlencode({
				'oiopu': self.file['oiopu'],
				'f': self.file['fileID'],
				'oiop': self.file['oiop'],
				'dl': self.file['dlselect'],
			})
			self.queue.appendleft({
				"filePartKey": "xml",
				"fileURL": url,
				"fileName": self.file['fileName']+'.key'
			})
		self.started.emit(self.file['fileKey'])
		self._next()
	
	def stop(self):
		if self.proc:
			self.proc.terminate()
			self.proc.deleteLater()
			self.proc = None
		if self.response:
			self.response.readyRead.disconnect()
			self.response.finished.disconnect()
			self.response.close()
			self.response.deleteLater()
			self.request = None
			self.response = None
		self.stopped.emit(self.file['fileKey'])
	
	def toggle(self):
		if self.response:
			self.stop()
		else:
			self.start()
	
	def _recalcTotalLength(self):
		size = 0
		for part in self.file['fileParts']:
			sz = part.get('total_length')
			if sz is None:
				sz = part['downloadKBCount']*1024
			size += sz
		self.file['total_length'] = size
	
	@Qt.pyqtSlot()
	def _partFinished(self):
		part = self.queue.popleft()
		logger.info("Download finished: %r", part['fileName'])
		if self.fh:
			if 'tmpFilePath' in part:
				self.fh.flush()
				try:
					os.rename(part['tmpFilePath'], part['filePath'])
				except OSError:
					logger.exception("Couldn't rename part file to %r", part['filePath'])
				self.done_length += self.fh.tell()
			else:
				if not self._parseXML(self.fh.getvalue().decode('ascii')):
					return
				if self.file['edv']:
					try:
						with open(self.file['keyPath'], 'w') as f:
							f.write(self.file['edv'])
							f.flush()
							try:
								xattr.setxattr(f.name, "user.sdm.oiopua", str(self.file['oiopua']))
								xattr.setxattr(f.name, "user.mime_type", 'text/x-kivuto-sdm-key')
							except OSError:
								logger.warn("Could not set extended attributes os file %r", f.name)
					except IOError:
						logger.exception("Couldn't create %r", self.file['keyPath'])
						self.stop()
						self.error.emit(self.file['fileKey'], 'Download.UnableToCreateFile', None)
						return
			self.fh.close()
		if self.response:
			self.response.deleteLater()
		self.fh = None
		self.request = self.response = None
		self._next()
	
	@Qt.pyqtSlot()
	def _metadatachange(self):
		self.response.metaDataChanged.disconnect()
		
		item = self.queue[0]
		if item['filePartKey'] == 'xml':
			return
		
		# Check status code
		status_code = int(self.response.attribute(Qt.QNetworkRequest.HttpStatusCodeAttribute))
		if status_code == 416:
			# At the end -> done
			self.fh.seek(0, 2)
			item['total_length'] = self.fh.tell()
			self._recalcTotalLength()
			self.response.close()
			return
		elif 300 <= status_code <= 999:
			self.response.close()
			self.stop()
			logger.error("Download of %r failed: %d %s", status_code, self.response.attribute(Qt.QNetworkRequest.HttpReasonPhraseAttribute))
			self.error.emit(self.file['fileKey'], 'Download.LinkUnavailableCallSupport')
			return False
		
		# Check Content-Range-Header
		content_range = self.response.rawHeader("Content-Range")
		if content_range:
			content_range = bytes(content_range[6:]).decode('ascii')
			start = int(content_range.split('-', 1)[0])
			self.fh.seek(start, 0)
			if '/*' not in content_range:
				total = int(content_range.split('/', 1)[1])
				item['total_length'] = total
				self._recalcTotalLength()
				# Preallocate some space
				fallocate(self.fh, FALLOC_FL_KEEP_SIZE, 0, item['total_length'])
			logger.info("Resuming %r at %d bytes", item['fileName'], self.fh.tell())
		else:
			self.fh.seek(0, 0)
			logger.info("Starting download of %r", item['fileName'])
			item['total_length'] = int(self.response.header(Qt.QNetworkRequest.ContentLengthHeader))
			self._recalcTotalLength()
			# Save total length and other metadata in xattr
			try:
				xattr.setxattr(self.fh.name, "user.sdm.total_length", str(item['total_length']))
				xattr.setxattr(self.fh.name, "user.xdg.origin.url", str(item['fileURL']))
				xattr.setxattr(self.fh.name, "user.mime_type", 'text/x-kivuto-sdm-sdc')
			except OSError:
				logger.warn("Could not set extended attributes on file %r", self.fh.name)
			# Preallocate some space
			fallocate(self.fh, FALLOC_FL_KEEP_SIZE, 0, item['total_length'])
	
	@Qt.pyqtSlot()
	def _read(self):
		part = self.queue[0]
		try:
			self.fh.write(self.response.readAll())
		except IOError:
			self.stop()
			self.error.emit(self.file['fileKey'], 'Download.InsufficientDiskSpace', None)
		
		if time.time()-self.tstamp > 1.2:
			self.tstamp = time.time()
			total = self.file['total_length']
			progress = self.done_length + self.fh.tell()
			logger.debug('Downloading %r (%d/%d; %.2f)', self.file['fileName'], total, progress, progress*100/total)
			self.progress.emit(self.file['fileKey'], progress*100/total)
	
	def _startConcat(self):
		self.proc = proc = Qt.QProcess(self)
		proc.setStandardOutputFile(self.file['tmpFilePath'])
		proc.error.connect(self._concatError)
		proc.finished.connect(self._concatFinished)
		proc.start(
			'cat',
			[p['filePath'] for p in self.file['fileParts']]
		)
		proc.closeWriteChannel()
		logger.info("Concatenating %r", self.file['fileName'])
		self.concatenating.emit(self.file['fileKey'])
	
	@Qt.pyqtSlot(int, Qt.QProcess.ExitStatus)
	def _concatFinished(self, exitCode, exitStatus):
		self.proc.deleteLater()
		self.proc = None
		if exitCode != 0:
			logger.info("Concatenation error %r: %d", self.file['fileName'], exitCode)
			self.stop()
			self.error.emit(self.file['fileKey'], 'Download.InsufficientDiskSpace', None)
		else:
			logger.info("Concatenated %r", self.file['fileName'])
			try:
				xattr.setxattr(self.file['tmpFilePath'], "user.mime_type", 'text/x-kivuto-sdm-sdc')
			except OSError:
				logger.warn("Could not set extended attributes on file %r", self.file['tmpFilePath'])
			os.rename(self.file['tmpFilePath'], self.file['filePath'])
			for partfile in self.file['fileParts']:
				if partfile['filePath'] != self.file['filePath']:
					os.unlink(partfile['filePath'])
					logger.info("Deleted %r", partfile['fileName'])
	
	@Qt.pyqtSlot(Qt.QProcess.ProcessError)
	def _concatError(self, processError):
		logger.info("Concatenation error %r: cat: %s", self.file['fileName'], self.proc.errorString())
		self.stop()
		self.error.emit(self.file['fileKey'], 'HTTPErrorDescriptions.Error', None)
		if self.proc:
			self.proc.deleteLater()
			self.proc = None
	
	def _startDecoding(self):
		self.proc = proc = Qt.QProcess(self)
		proc.error.connect(self._decodeError)
		proc.finished.connect(self._decodeFinished)
		proc.readyRead.connect(self._decodeStatus)
		proc.start(
			'xsdm',
			['--', self.file['filePath']]
		)
		proc.closeWriteChannel()
		logger.info("Decoding %r", self.file['fileName'])
		self.decoding.emit(self.file['fileKey'])
	
	@Qt.pyqtSlot(int, Qt.QProcess.ExitStatus)
	def _decodeFinished(self, exitCode, exitStatus):
		self.proc.deleteLater()
		self.proc = None
		if exitCode != 0:
			logger.info("Decoding error %r: %d", self.file['fileName'], exitCode)
			self.stop()
			self.error.emit(self.file['fileKey'], 'Download.UnpackingError', None)
		else:
			logger.info("Decoded %r", self.file['fileName'])
			self.file['finished'] = True
			try:
				xattr.setxattr(self.file['filePath'], "user.sdm.extracted", str(int(time.time())))
				xattr.setxattr(self.file['filePath'], "user.sdm.extracted_path", str(self.file.get('decodedDirectory', '')))
			except OSError:
				logger.warn("Could not set extended attributes on %r", self.file['fileName'])
			self.finished.emit(self.file['fileKey'])
	
	@Qt.pyqtSlot()
	def _decodeStatus(self):
		data = bytes(self.proc.readAll())
		sys.stdout.write(data.decode(errors='surrogateescape'))
		data = data.replace(b'\r', b'\n')
		for line in data.split(b'\n'):
			if line.startswith(b' [#'):
				self.progress.emit(self.file['fileKey'], min(line.count(b'#')*100/6, 99))
			match = re.search(rb"Creating directory structure at '(.*)'", line)
			if match:
				self.file['decodedDirectory'] = match.group(1).decode(errors='surrogateescape')
	
	@Qt.pyqtSlot(Qt.QProcess.ProcessError)
	def _decodeError(self, processError):
		logger.info("Decoding error %r: xsdm: %s", self.file['fileName'], self.proc.errorString())
		self.stop()
		if processError == Qt.QProcess.FailedToStart:
			self.error.emit(self.file['fileKey'], None, 'xSDM not found. Please download, compile and install it from https://github.com/v3l0c1r4pt0r/xSDM .')
		else:
			self.error.emit(self.file['fileKey'], 'Download.UnpackingError', None)
		if self.proc:
			self.proc.deleteLater()
			self.proc = None
	
	def _next(self):
		# Check if done downloading
		if not self.queue:
			# Concatenate?
			if not os.path.exists(self.file['filePath']):
				if self.file['fileName'] != self.file['fileParts'][0]['fileName']:
					self._startConcat()
				else:
					logger.error("File %r missing?! Cannot continue.", self.file['fileName'])
					self.stop()
					self.error.emit(self.file['fileKey'], 'Download.UnpackingError', None)
			else:
				self._startDecoding()
			return
		# Get next queue item
		item = self.queue[0]
		# Construct request
		self.request = request = Qt.QNetworkRequest(Qt.QUrl(item['fileURL']))
		request.setRawHeader("Accept", "*/*")
		# Open destination file
		if item['filePartKey'] == 'xml':
			request.setPriority(Qt.QNetworkRequest.HighPriority)
			self.fh = io.BytesIO()
		else:
			try:
				# Check if already done
				if os.path.exists(item['filePath']):
					logger.info("Skipping %r", item['fileName'])
					self.done_length += item.get('total_length')
					self._partFinished()
					return
				# Check for partial downloads
				elif os.path.exists(item['tmpFilePath']):
					self.fh = fh = open(item['tmpFilePath'], 'rb+')
					fh.seek(0, 2)
					pos = fh.tell()
					pos = int(pos//4096*4096)
					fh.seek(pos, 0)
					request.setRawHeader("Range", "bytes={0}-".format(pos))
					logger.info("Trying resume %r at %d bytes", item['fileName'], pos)
					if pos > 1:
						os.posix_fadvise(fh.fileno(), 0, pos-1, os.POSIX_FADV_DONTNEED)
					os.posix_fadvise(fh.fileno(), pos, 0, os.POSIX_FADV_NOREUSE)
				# Else start anew
				else:
					logger.info("Downloading %r", item['fileName'])
					self.fh = open(item['tmpFilePath'], 'xb')
					os.posix_fadvise(self.fh.fileno(), 0, 0, os.POSIX_FADV_NOREUSE)
			except IOError:
				logger.exception("Couldn't create %r", item['tmpFilePath'])
				self.stop()
				self.error.emit(self.file['fileKey'], 'Download.UnableToCreateFile', None)
				return
		# Fire request
		self.response = response = self.networkaccess.get(request)
		response.metaDataChanged.connect(self._metadatachange)
		response.readyRead.connect(self._read)
		response.finished.connect(self._partFinished)

	def _parseXML(self, xml: str) -> bool:
		root = ET.fromstring(xml)
		for child in root:
			if child.tag == 'oiopua':
				self.file['oiopua'] = child.text.strip()
			elif child.tag == 'edv':
				self.file['edv'] = child.text.strip()
			elif child.tag == 'errorTextKey':
				error = "".join(child.itertext()).strip()
				if error:
					self.stop()
					self.error.emit(self.file['fileKey'], error, None)
					return False
		return True
	
	progress = Qt.pyqtSignal(int, float)
	started = Qt.pyqtSignal(int)
	stopped = Qt.pyqtSignal(int)
	concatenating = Qt.pyqtSignal(int)
	decoding = Qt.pyqtSignal(int)
	finished = Qt.pyqtSignal(int)
	error = Qt.pyqtSignal(int, str, str)

class SDMViewHandler(Qt.QObject):
	
	class JSObject(Qt.QObject):
		@Qt.pyqtSlot(int, Qt.QVariant, result=bool)
		def UICommand(self, cmdid, value):
			if cmdid == 0:
				self.downloadClicked.emit(int(value))
			elif cmdid == 6:
				self.dirChooserRequested.emit()
			elif cmdid == 7:
				self.runClicked.emit(int(value))
			else:
				logger.info("command %d (%r)", cmdid, value)
			return True
		
		@Qt.pyqtSlot(str)
		@Qt.pyqtSlot(str, str)
		@Qt.pyqtSlot(str, str, str)
		@Qt.pyqtSlot(str, str, str, str)
		def open(self, url=None, name=None, specs=None, replace=None):
			self.windowOpenRequested.emit(url, name, specs, replace)
		
		downloadClicked = Qt.pyqtSignal(int)
		runClicked = Qt.pyqtSignal(int)
		dirChooserRequested = Qt.pyqtSignal()
		windowOpenRequested = Qt.pyqtSignal(str, str, str, str)
	
	def __init__(self, parent, frame: Qt.QWebFrame) -> None:
		super().__init__(parent)
		self.frame = frame
		self.jsobj = jsobj = self.JSObject(self)
		self.files = []
		self.downloadDir = getDownloadDir()
		self.dirChooserRequested = jsobj.dirChooserRequested
		self.hfBaseUrl = None
		self.networkaccess = Qt.QNetworkAccessManager(self)
		self.downloads = {}
		
		frame.javaScriptWindowObjectCleared.connect(self._initJS)
		frame.initialLayoutCompleted.connect(self._onReady)
		jsobj.downloadClicked.connect(self._onDownloadClick)
		jsobj.runClicked.connect(self._onRunClick)
		jsobj.windowOpenRequested.connect(self._onWindowOpen)
	
	def abortAll(self):
		for download in self.downloads.values():
			download.stop()
			download.deleteLater()
		self.downloads.clear()
	
	@Qt.pyqtSlot()
	def _initJS(self) -> None:
		self.frame.addToJavaScriptWindowObject("uiObject", self.jsobj)
		self.frame.evaluateJavaScript('window.open = uiObject.open;')
		self.frame.evaluateJavaScript('requestAttempt = function(n){};')
	
	def _parseFileInfo(self, xml):
		files = []
		root = ET.fromstring(xml)
		for child in root:
			if child.tag == 'File':
				parts = []
				fdata = {
					"fileKey": int(child.attrib['FileKey']),
					"fileTypeID": int(child.attrib['FileTypeID']),
					"fileName": child.attrib['FileName'],
					"fileParts": parts
				}
				files.append(fdata)
				for part in child:
					if part.tag == 'FilePart':
						parts.append({
							"filePartKey": int(part.attrib['FilePartKey']),
							"fileName": part.attrib['FileName'],
							"fileURL": part.attrib['FileURL'],
							"downloadKBCount": int(part.attrib['DownloadKBCount']),
							"extractedKBCount": int(part.attrib['ExtractedKBCount']),
						})
		return files
	
	
	@Qt.pyqtSlot()
	def _onReady(self) -> None:
		hfBaseUrl = self.frame.findFirstElement('#hfBaseUrl').attribute('value')
		self.hfBaseUrl = urllib.parse.urljoin(self.frame.baseUrl().toString(), hfBaseUrl)
		if not hfBaseUrl:
			return
		# Find all file information
		del self.files[:]
		self.files = self._parseFileInfo(self.frame.findFirstElement('#hfFileInfo').attribute('value'))
		for download in self.files:
			i = download['fileKey']
			dlselect = self.frame.findFirstElement('#dlSelect{0}'.format(i))
			if dlselect.isNull():
				dlselect = self.frame.findFirstElement('#dlSelect1')
			download['hfBaseUrl'] = self.hfBaseUrl
			download['dlselect'] = dlselect.attribute('value')
			download['oiop'] = self.frame.findFirstElement('#oiop{0}'.format(i)).attribute('value')
			download['oiopu'] = self.frame.findFirstElement('#oiopu{0}'.format(i)).attribute('value')
			download['oiopua'] = self.frame.findFirstElement('#oiopua{0}'.format(i)).attribute('value')
			download['fileID'] = self.frame.findFirstElement('#fileID{0}'.format(i)).attribute('value')
			download['edv'] = self.frame.findFirstElement('#edv{0}'.format(i)).attribute('value')

		# Set Download Folder
		self.setFolderSelection(self.downloadDir)
	
	@Qt.pyqtSlot(str)
	def _onWindowOpen(self, url):
		url = urllib.parse.urljoin(self.frame.baseUrl().toString(), url)
		self.frame.page().linkClicked.emit(Qt.QUrl(url))
	
	def _findPerformedDownloads(self):
		self.jsFunc('setInitialFileProperties')
		for download in self.files:
			fileKey = download['fileKey']
			path = os.path.join(self.downloadDir, download['fileName'])
			keypath = path + '.key'
			download['filePath'] = path
			download['tmpFilePath'] = path+'.part'
			download['keyPath'] = keypath
			
			# Alles auf null setzen
			self.jsFunc('clearError', fileKey)
			self.jsFunc('resetEstimateValues', fileKey)
			self.jsFunc('setButtonStopped', fileKey)
			self.setButtonAction(fileKey, 'Download.Start')
			self.jsFunc('setButtonTitle', fileKey, "Download.StartTitle")
			self.setStatus(fileKey, 'Download.Start')
			download['edv'] = ''
			download['oiopua'] = ''
			if 'finished' in download:
				del download['finished']
			if 'decodedDirectory' in download:
				del download['decodedDirectory']
			
			# ggf. Keyfile auslesen
			if os.path.exists(keypath):
				with open(keypath, 'r') as f:
					download['edv'] = f.read().strip()
				try:
					download['oiopua'] = xattr.getxattr(keypath, "user.sdm.oiopua").decode()
				except OSError:
					logger.warn("Download keyfile %r has no extended attributes", keypath)
			
			# ggf. vollständigen Download finden
			if os.path.exists(path):
				self.setProgress(fileKey, 100)
				self.pause(fileKey)
				download['total_length'] = os.stat(path).st_size
				
				try:
					download['finished'] = bool(xattr.getxattr(path, "user.sdm.extracted"))
					download['decodedDirectory'] = xattr.getxattr(path, "user.sdm.extracted_path").decode()
					if download['finished']:
						self.setProgressDone(fileKey)
						self.setStatus(fileKey, "Download.Done")
						self.jsFunc('setCompleteDownloadState', fileKey)
				except OSError:
					pass
				
				continue
			
			# ggf. unvollständige Downloads finden
			total_length = 0
			downloaded_length = 0
			for part in download['fileParts']:
				path = os.path.join(self.downloadDir, part['fileName'])
				part['filePath'] = path
				part['tmpFilePath'] = tmppath = path+'.part'
				if os.path.exists(path):
					part['total_length'] = os.stat(path).st_size
					total_length += part['total_length']
					downloaded_length += part['total_length']
				elif os.path.exists(tmppath):
					downloaded_length += os.stat(tmppath).st_size
					try:
						part['total_length'] = int(xattr.getxattr(tmppath, "user.sdm.total_length").decode())
						total_length += part['total_length']
					except OSError:
						logger.warn("Incomplete download %r has no extended attributes", tmppath)
						total_length += part['downloadKBCount']*1024
				else:
					total_length += part['downloadKBCount']*1024
			
			if downloaded_length > 0:
				self.setProgress(fileKey, min(downloaded_length*100/total_length, 99))
				self.pause(fileKey)

	@Qt.pyqtSlot(int)
	def _onDownloadClick(self, id: int) -> None:
		if id < 1 or id > len(self.files):
			return

		file = self.files[id-1]
		if file['fileKey'] in self.downloads:
			self.downloads[file['fileKey']].toggle()
		else:
			self.downloads[file['fileKey']] = dl = Downloader(self, file)
			dl.started.connect(self.resume)
			dl.stopped.connect(self.pause)
			dl.concatenating.connect(self._onConcat)
			dl.decoding.connect(self._onDecode)
			dl.finished.connect(self._onFinish)
			dl.error.connect(self._onError)
			dl.stopped.connect(self.pause)
			dl.progress.connect(self.setProgress)
			self.downloads[file['fileKey']].start()
	
	@Qt.pyqtSlot(int)
	def _onRunClick(self, id: int) -> None:
		if id < 1 or id > len(self.files):
			return

		file = self.files[id-1]
		path = self.downloadDir
		subdir = file.get('decodedDirectory', '').lstrip('/')
		if subdir:
			path = os.path.join(path, subdir)
		subprocess.call(['xdg-open', path])
	
	def jsFunc(self, name: str, *args):
		args = ', '.join(json.dumps(a) for a in args)
		return self.frame.evaluateJavaScript('{}({})'.format(name, args))
	
	@Qt.pyqtSlot(str)
	def setFolderSelection(self, path: str):
		self.downloadDir = path
		self.jsFunc('setFolderSelection', path)
		self._findPerformedDownloads()
	
	@Qt.pyqtSlot(int, float)
	def setProgress(self, download_id, percent):
		self.jsFunc('setProgress', download_id, percent)

	@Qt.pyqtSlot(int)
	def pause(self, download_id):
		self.jsFunc('pause', download_id)

	@Qt.pyqtSlot(int)
	def resume(self, download_id):
		self.jsFunc('clearError', download_id)
		self.jsFunc('resume', download_id)

	def setStatus(self, download_id: int, status_key: str) -> None:
		self.jsFunc('setStatus', download_id, status_key)
	
	def setButtonAction(self, download_id: int, status_key: str) -> None:
		self.jsFunc('setButtonAction', download_id, status_key)
	
	def setProgressDone(self, download_id: int):
		self.jsFunc('setProgressDone', download_id)
	
	#setStatus, setProgress, setError, setFileOutputFolderText, setFolderSelection, setProgressDone, setProgressDownloading, setProgressError

	@Qt.pyqtSlot(int)
	def _onConcat(self, download_id):
		self.setProgressDone(download_id)
		self.setButtonAction(download_id, 'Download.CRCChecking')

	@Qt.pyqtSlot(int)
	def _onDecode(self, download_id):
		self.setProgress(download_id, 0)
		self.setStatus(download_id, 'Download.Unpacking')
		self.setButtonAction(download_id, 'Download.Unpacking')

	@Qt.pyqtSlot(int)
	def _onFinish(self, download_id):
		self.setProgress(download_id, 100)
		self.pause(download_id)
		self.setProgressDone(download_id)
		self.setStatus(download_id, "Download.Done")
		self.jsFunc('setCompleteDownloadState', download_id)
		self.downloads[download_id].deleteLater()
		del self.downloads[download_id]

	@Qt.pyqtSlot(int, str, str)
	def _onError(self, download_id, error, errorText):
		if error:
			self.jsFunc('setErrorUsingTextItemKey', download_id, error)
		else:
			self.jsFunc('setError', download_id, errorText)
		self.setButtonAction(download_id, 'Download.Resume')

class MainWindow(QtGui.QMainWindow):
	
	def __init__(self, url: URL) -> None:
		super().__init__()
		self.ui = mainwindow.Ui_MainWindow()
		self.ui.setupUi(self)
		
		self.handler = SDMViewHandler(self, self.ui.webView.page().mainFrame())
		self.handler.dirChooserRequested.connect(self.showDirChooser)
		
		self.url = url
		
		self.ui.webView.settings().setAttribute(Qt.QWebSettings.XSSAuditingEnabled, True)
		self.ui.webView.settings().setAttribute(Qt.QWebSettings.SpatialNavigationEnabled, True)
		page = self.ui.webView.page()
		page.setLinkDelegationPolicy(Qt.QWebPage.DelegateAllLinks)
		page.linkClicked.connect(self.openUrl)
		
		data = b'<html><body><div style="text-align:center;position:fixed;top:50%;margin-top:-16px;width:100%"><img src="data:image/gif;base64,'+loadergif+b'"/></div></body></html>'
		self.ui.webView.setContent(data, 'text/html;charset=utf-8')
		Qt.QMetaObject.invokeMethod(self, "_loadUrl", Qt.Qt.QueuedConnection)
	
	@Qt.pyqtSlot()
	def _loadUrl(self):
		self.ui.webView.load(Qt.QUrl(self.url))
	
	@Qt.pyqtSlot(Qt.QUrl)
	def openUrl(self, url: Qt.QUrl) -> None:
		webbrowser.open(url.toString())
	
	@Qt.pyqtSlot()
	def showDirChooser(self):
		dialog = QtGui.QFileDialog(self)
		dialog.setFileMode(QtGui.QFileDialog.Directory)
		dialog.setOption(QtGui.QFileDialog.ShowDirsOnly)
		dialog.setDirectory(self.handler.downloadDir)
		dialog.show()
		dialog.fileSelected.connect(self.handler.setFolderSelection)
		dialog.fileSelected.connect(self.saveFolderSelection)
		dialog.done.connect(dialog.deleteLater)
	
	def saveFolderSelection(self, path: str) -> None:
		settings = Qt.QSettings('cg909', 'xSDM-qt')
		settings.setValue('downloadDir', path)
	
	def closeEvent(self, event: QtGui.QCloseEvent) -> None:
		try:
			self.handler.abortAll()
		except Exception:
			pass
		super().closeEvent(event)

class Application(QtGui.QApplication):
	
	def __init__(self, argv: List[str]) -> None:
		super().__init__(argv)
		self.mainWindow = None
		self.setOrganizationName('cg909')
		self.setApplicationName('"Secure" Download Manager')
		Qt.QMetaObject.invokeMethod(self, "_run", Qt.Qt.QueuedConnection)
	
	@Qt.pyqtSlot()
	def _run(self) -> None:
		geometry = None
		windowState = None
		
		settings = Qt.QSettings('cg909', 'xSDM-qt')
		downloadDir = settings.value('downloadDir')
		if downloadDir:
			setDownloadDir(downloadDir)
		
		if self.isSessionRestored():
			sessionid = self.sessionId()
			try:
				path = os.path.join('/var/tmp/', 'qxsdm-{}'.format(os.getuid()), '{}.session'.format(sessionid))
				with open(path, 'r', encoding="UTF-8") as f:
					data = json.load(f)
				file = data['file']
				url = data['url']
				setDownloadDir(data['downloadDir'])
				geometry = b64decode(data['geometry'])
				windowState = b64decode(data['windowState'])
			
			except FileNotFoundError:
				logging.error('Could not restore session %r. File %s is missing.', sessionid, path)
				sys.exit(66)
			except Exception:
				logger.exception('Error while restoring session file %s. Aborting.', path)
				sys.exit(70)
			else:
				logging.info("Session %r restored", sessionid)
		else:
			arguments = self.parseCmdline(self.arguments())
			
			if not arguments.file:
				file = QtGui.QFileDialog.getOpenFileName(
					None,
					"Open File",
					getXDGDownloadDir(),
					"Dreamspark-Bestellpakete (*.sdx)")
				if not file:
					return self.exit(0)
			else:
				file = arguments.file
			
			try:
				url = self.readSDXFile(file)
			except (FileNotFoundError, PermissionError) as e:
				logger.error('%s: %s [%d]', e.filename, e.strerror, e.errno)
				return self.exit(66)
			except IOError as e:
				logger.error('%s: %s [%d]', e.filename, e.strerror, e.errno)
				return self.exit(74)
		
		self.mainWindow = MainWindow(url)
		if geometry:
			self.mainWindow.restoreGeometry(geometry)
		if windowState:
			self.mainWindow.restoreState(windowState)
		self.mainWindow.show()
		self.mainWindow.setWindowFilePath(file)

	def readSDXFile(self, path: str) -> URL:
		logger.debug('checking SDX file %r', path)
		f = open(path, 'r')
		url = f.read(4096).strip()
		logger.debug('parsing contents')
		result = urlparse(url)
		logger.debug('checking url')
		if result.scheme in ('http', 'https') and 'WebStore/Account/SdmAuthorize.aspx' in result.path:
			logger.debug('valid url: %r', url)
			return url
		else:
			raise ValueError('file doesn\'t contain an URL')
	
	@staticmethod
	def parseCmdline(argv: List[str]) -> Any:
		parser = argparse.ArgumentParser(description='Alternative for "Secure Download Manager"')
		parser.add_argument('file', type=str, nargs='?', help='.sdx file')
		parser.add_argument('--help-qt', action="help", help='show Qt specific options')
		qtgroup = parser.add_argument_group('Qt specific arguments')
		qtgroup.add_argument('-style', metavar="name", type=str, help='set application GUI style')
		qtgroup.add_argument('-stylesheet', metavar="path", type=str, help='set application styleSheet')
		qtgroup.add_argument('-widgetcount', action="store_true", help='debug messages about widget statistics')
		qtgroup.add_argument('-reverse', action="store_true", help='switch between LTR/RTL design')
		qtgroup.add_argument('-qmljsdebugger', metavar="portspec", type=str, help='activate the QML/JS debugger with a specified port.')
		return parser.parse_args()

	def saveState(self, manager: Qt.QSessionManager) -> None:
		if self.mainWindow:
			path = os.path.join('/var/tmp/', 'qxsdm-{}'.format(os.getuid()))
			if not os.path.exists(path):
				os.mkdir(path, mode=0o700)
			path = os.path.join(path, '{}.session'.format(manager.sessionId()))
			self.saveSessionStateFile(path)
			manager.setDiscardCommand(['/bin/rm', path])
			logger.info("Saved session %r", manager.sessionId())
			logger.info("Restore with %r", ' '.join(manager.restartCommand()))

	def saveSessionStateFile(self, path: str) -> None:
		data = {
			'path': self.mainWindow.windowFilePath(),
			'url': self.mainWindow.url,
			'downloadDir': self.mainWindow.handler.downloadDir,
			'geometry': b64encode(self.mainWindow.saveGeometry()).decode('ascii'),
			'windowState': b64encode(self.mainWindow.saveState()).decode('ascii')
		}
		with open(path, 'x', encoding="UTF-8") as f:
			json.dump(data, f, indent=True, sort_keys=True)
			f.flush()

def main(argv: List[str]) -> int:
	# Initialize logging
	logging.basicConfig(level=logging.INFO)
	e = ErrorDialogHandler()
	logger.addHandler(e)
	# Initialize Application
	app = Application(argv)
	# Run until closed
	return app.exec_()

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
